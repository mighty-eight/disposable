require('./../../src/testfile-md5')(__filename, "UNIT TEST");

var traverse   = require('./../../src/traverse'),
    htmlentity = require('./../../src/htmlentity'),
    trans      = {},
    obj        = require(__dirname + '/../props/traverse.json');

function ucase(input) {
    if (typeof input === 'string') {
        return input.toLocaleUpperCase();
    } else {
        return input;
    }
}

trans = traverse(obj);

console.log('AFTER');
console.log(trans);

describe('ucase function', function () {
    it('does UPPER CASE', function () {
        expect(ucase('upper case')).toBe('UPPER CASE');
        expect(ucase('uPpeR caSe')).toBe('UPPER CASE');
    });
    it('non strings', function () {
        expect(ucase(1234)).toBe(1234);
        expect(JSON.stringify(ucase({keytype: 'object', two: 2}))).toBe(JSON.stringify({keytype: 'object', two: 2}));
    });
});


console.log(traverse(obj, ucase));