'use strict';

var headers     = require('./docs/examples/headers.json'),
    particulars = require('./package.json'),
    port        = 9000,
    express     = require('express'),
    router      = express.Router(),
    app         = express(),
    bodyParser  = require('body-parser'),
    content     = {
        minified: require('./docs/examples/curl.json'),
        pretty: require('./docs/examples/tidy.json')
    };

function hyperlink(urlpath, name) {
    return '<a href="http://' +urlpath + '">'+name+'</a>';
}

headers['app-n'] = particulars.name;
headers['app-v'] = particulars.version;

app.use(function(req, res, next) {
    for (var k in headers) res.setHeader(k, headers[k]);
    // removing express footprint
    res.removeHeader('X-Powered-By'); // good practice
    return next();
});

router.get('/', function(req, res){
    res.json({
        path: '/api/',
        error: false,
        message: {
            raw: 'http://localhost:'+port+'/api/raw',
            augmented: 'http://localhost:'+port+'/api/augmented'
        }
    });
});

router.get('/raw', function(req, res){
    res.json({
        path: '/api/',
        error: false,
        message: {
            raw: 'http://localhost:'+port+'/api/raw',
            augmented: 'http://localhost:'+port+'/api/augmented'
        }
    });
});

router.get('raw', function(req, res){
    res.json({
        path: '/api/raw',
        error: false,
        message: {
            data: content.minified
        }
    });
});

router.get('api', function(req, res){
    res.json({
        path: '/api/augmented',
        error: false,
        message: {
            data: {}
        }
    });
});

app.use('/api', router);

console.info(particulars.name + ' is running on port ' + port);
console.info('version: ' + particulars.version);
console.info('Open: http://127.0.0.1:' + port +'/');
app.listen(port);

