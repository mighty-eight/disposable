'use strict';

var primitives = {
    string: true,
    number: true
};

function typeOf(input) {
    return typeof input;
}

function isPrimitive(input) {
    return !(!primitives[typeOf(input)])
}

function traverse(myobject, transformer) {
    var k;
    if (typeOf(myobject) === 'object') {
        for (k in myobject) {
            //console.info('Key ' + k);
            //console.info('Val ' + myobject[k]);
            if (isPrimitive(myobject[k]) === true) {
                //console.info('Trns ' + transformer(myobject[k]));
                myobject[k] = transformer(myobject[k]);
            }
        }
    } else {

    }
    return myobject;
}

(function(){
    /**
     * Function wrapper
     * @param input
     * @returns _jacin
     */
    module.exports = function (input, transformer) {
        console.log(input);
        console.log(transformer);
        var returned = JSON.parse(JSON.stringify(input));
        return traverse(returned, transformer);
    };
})();
