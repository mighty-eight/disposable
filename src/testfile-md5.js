'use strict';

var md5File = require('md5-file'),
    _last, _file, _sep = '/',
    color   = require('bash-color');

(function(){
    module.exports = function (testfile, testtype) {
        _sep  = (testfile.indexOf(_sep) < 0) ? '\\' : _sep;
        _file = testfile.split(_sep);
        _last = _file[(_file.length - 1)];
        console.log(
            'Type: ' +
            color.wrap(testtype, color.colors.YELLOW, color.styles.bold) +
            ' md5: ' +
            color.wrap(md5File.sync(testfile), color.colors.BLUE, color.styles.bold) +
            ' file: ' +
            color.wrap(_last, color.colors.PURPLE, color.styles.bold)
        );
        return;
    };

})();
