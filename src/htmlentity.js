'use strict';

var md5 = require('md5'),
    newString, chk2, chk;

/**
 * Overloading String() prototype
 * @returns {string} Encoded string
 */
String.prototype.toHtmlEntities = function() {
    return this.replace(/./gm, function(s) {
        return '&#' + s.charCodeAt(0) + ';';
    });
};

/**
 * Overloading String() prototype
 * @param string     HTML Entities encoded
 * @returns {string} Decoded string
 */
String.prototype.fromHtmlEntities = function(string) {
    return (string+'').replace(/&#\d+;/gm,function(s) {
        return String.fromCharCode(s.match(/\d+/gm)[0]);
    });
};

function isString(input) {
    var types = {
        number: true,
        string: true
    };
    return (types[typeof input] === true);
    return typeof input !== 'string' || typeof input !== 'number';
}

function encode(input) {
    if (!isString(input)) {
        return '';
    }
    var raw = (new String(input));
    return raw.toHtmlEntities();
}

function decode(input) {
    if (!isString(input)) {
        return '';
    }
    var enc = (new String(''));
    return enc.fromHtmlEntities(input);
}

function isHtmlEntity(input) {
    if (!isString(input)) {
        return false;
    }
    return input === encode(decode(input));
}

function HtmlEntity() {

    this.is = function (string) {
        return isHtmlEntity(string);
    };

    this.string = function(input) {
        return isString(input);
    };

    this.encode = function(string) {
        if (isHtmlEntity(string)) {
            return string;
        }
        return encode(string);
    };

    this.decode = function(string) {
        return decode(string);
    };
}

module.exports = new HtmlEntity();